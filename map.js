class GoogleMap {
    constructor(mapElement, restaurantElement, config) {
        this._myPos = null
        this._config = config
        this.mapElement = mapElement
        this.restaurantElement = restaurantElement
        this._init()
    }

    _init = () => {
        const { zoom, defaultCenter } = this._config
        const initialCenter = new google.maps.LatLng(defaultCenter.lat, defaultCenter.lng)

        this.infoWindow = new google.maps.InfoWindow()
        this.map = new google.maps.Map(this.mapElement, { center: initialCenter, zoom })
        this.service = new google.maps.places.PlacesService(this.map)

        this._getCurrentLocation()
        this._searchNearByPlaces(initialCenter)

        this.map.addListener('bounds_changed', () => {
            const { lat, lng } = this.map.getCenter()
            this._searchNearByPlaces(new google.maps.LatLng(lat(), lng()))
        })
        this._option = 'name'
        this._sortBy = {
            distance : (a, b) => a.distance > b.distance ? 1 : -1,
            name: (a, b) => a.name > b.name ? 1 : -1,
            rating: (a, b) => a.rating > b.rating ? 1 : -1
        }
    }

    /**
     * Set Option
     * @param {string} option, [name | distance | rating]
     */
    setOption = (option) => {
        this._option = option
    }

    getOption = () => {
        return this._option
    }

    refreshList = () => {
        const { lat, lng } = this.map.getCenter()
        this._searchNearByPlaces(new google.maps.LatLng(lat(), lng()))
    }

    _searchNearByPlaces = (location) => {
        const { radius, type } = this._config
        this.service.nearbySearch({ location, radius, type }, (results, status) => {
            if (status !== google.maps.places.PlacesServiceStatus.OK) {
                this.restaurantElement.innerHTML = ''
            }

            const places = results && results.map((place) => {
                this._createMarker(place)
                const { lat, lng } =  place.geometry.location
                const distance = (lat && lng) ? this._calculateDistance(lat(), lng()) : null
            
                return ({
                    ...place,
                    distance
                })
            })

            this.restaurantElement.innerHTML = places && places.sort(this._sortBy[this._option]).reduce((result, place) => result += `<li>${place.name}</li>`, '')
        });
    }

    _getCurrentLocation = () => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition((position) => {
                this._myPos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                }
    
                this.map.setCenter(this._myPos)

                const marker = new google.maps.Marker({
                    map: this.map,
                    position: this._myPos,
                    draggable: true
                })

                marker.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png')
                marker.addListener('drag', (event) => {
                    const { lat, lng } = event.latLng;
                    this._myPos = {
                        lat: lat(),
                        lng: lng()
                    }
                    this._searchNearByPlaces(new google.maps.LatLng(lat(), lng()))
                });
            }, () => {
                this._handleLocationError(true)
            })
        } else {
            this._handleLocationError(false)
        }
    }

    _handleLocationError = (browserHasGeolocation, pos) => {
        this.infoWindow.setPosition(this.map.getCenter())
        this.infoWindow.setContent(browserHasGeolocation ?
                              'Error: The Geolocation service failed.' :
                              'Error: Your browser doesn\'t support geolocation.')
        this.infoWindow.open(this.map)
    }

    _createMarker = (place) => {
        const marker = new google.maps.Marker({
          map: this.map,
          position: place.geometry.location
        })
    
        google.maps.event.addListener(marker, 'click', (e) => {
          this.infoWindow.setContent(
                `<div class="restaurant-pin-container">
                    <h2>${place.name}</h2> 
                    <h4>${place.vicinity}</h4>
                    <span>Rating: ${place.rating}</span>
                </div>`
          )
          this.infoWindow.open(this.map, marker)
        })
    }

    /**
     * Calculate Distance between my position and restaurant position 
     * @param {number} toLat 
     * @param {number} toLng 
     * @return {number} unit: meters
     */
    _calculateDistance = (toLat, toLng) => {
        return this._myPos && google.maps.geometry.spherical.computeDistanceBetween(
        new google.maps.LatLng(this._myPos.lat, this._myPos.lng), new google.maps.LatLng(toLat, toLng))
    }

}

window.onload = () => {
    const restaurantElement = document.querySelector('#restaurant-container')
    const mapElement = document.querySelector('#map')
    const config = {
        radius: 500,
        type: 'restaurant',
        zoom: 18,
        defaultCenter: {
            lat: 22.319,
            lng: 114.169
        }
    }

    const googleMap = new GoogleMap(mapElement, restaurantElement, config);
    document.querySelector('#sortBy').addEventListener('change', function() {
        this.value && googleMap.setOption(this.value)
        googleMap.refreshList()
    });
}

